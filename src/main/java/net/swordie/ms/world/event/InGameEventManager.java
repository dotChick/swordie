package net.swordie.ms.world.event;

import net.swordie.ms.Server;
import net.swordie.ms.ServerConfig;
import net.swordie.ms.client.character.BroadcastMsg;
import net.swordie.ms.client.character.Char;
import net.swordie.ms.connection.packet.WvsContext;
import net.swordie.ms.enums.ChatType;
import net.swordie.ms.handlers.EventManager;

import java.util.HashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class InGameEventManager {

    public static final int REGISTRATION_DURATION_MINS = ServerConfig.DEBUG_MODE ? 1 : 5; // devs want fast
    private static InGameEventManager instance = new InGameEventManager();
    private HashMap<InGameEventType, InGameEvent> events = new HashMap<>();
    private ScheduledFuture schedule;
    private ScheduledFuture reminderTimer;
    private InGameEventType previousEvent;
    private int remindersSent = 0;

    public InGameEventManager() {
        events.put(InGameEventType.RussianRoulette, new RussianRouletteEvent());
        events.put(InGameEventType.PinkZakumBattle, new PinkZakumEvent());
        // more to come...

        if (!ServerConfig.DEBUG_MODE)
            schedule = EventManager.addFixedRateEvent(this::doEvent, 5, 40, TimeUnit.MINUTES);
    }

    public static InGameEventManager getInstance() {
        return instance;
    }

    public void forceNextEvent() { // for testing only, this should not be used in prod
        if (ServerConfig.DEBUG_MODE) {
            InGameEvent curEvent = getActiveEvent();
            if (curEvent != null) {
                curEvent.endEvent();
            }
            doEvent();
        } // else notify character that server is in prod
    }

    private void doEvent() {
        InGameEvent event = events.entrySet().stream()
                .filter(e -> e.getValue().getEventType() != previousEvent)
                .findFirst().get().getValue();

        previousEvent = event.getEventType();

        event.clear(); // reset map info for next run
        if (!ServerConfig.DEBUG_MODE) {
            reminderTimer = EventManager.addFixedRateEvent(this::sendReminder, 30, 60, TimeUnit.SECONDS);
        }

        Server.getInstance().getWorldById(ServerConfig.WORLD_ID)
                .broadcastPacket(WvsContext.broadcastMsg( BroadcastMsg.notice("Sự kiện " +event.getEventName() + " đã bắt đầu cho đăng ký! Thời gian để đăng ký là: " + REGISTRATION_DURATION_MINS + " phút.")));
        event.doEvent();
    }

    private void sendReminder() {
        Server.getInstance().getWorldById(ServerConfig.WORLD_ID)
                .broadcastPacket(WvsContext.broadcastMsg(BroadcastMsg.notice("Sự kiện " + getActiveEvent().getEventName() + " sắp bắt đầu, nhanh tay tham dự nào!")));
        remindersSent += 1;
        if (remindersSent >= 4) // = 5 minutes
            reminderTimer.cancel(true);
    }

    public InGameEvent getOpenEvent() {
        InGameEvent e = null;
        for (InGameEvent ige : events.values())
            if (ige.isOpen()) {
                e = ige;
            }
        return e;
    }

    public InGameEvent getActiveEvent() {
        for (InGameEvent ige : events.values())
            if (ige.isActive()) {
                return ige;
            }
        return null;
    }

    public void joinPublicEvent(Char c) {
        InGameEvent e = getActiveEvent();

        if (e == null) {
            c.chatMessage(ChatType.SystemNotice, "Hiện tại không có sự kiện nào đang diễn ra!");
        } else if (!e.isOpen()) {
            c.chatMessage(ChatType.SystemNotice, "Sự kiện đã kết thúc đăng ký.");
        } else {
            e.joinEvent(c);
        }
    }

    public InGameEvent getEvent(InGameEventType type) {
        for (InGameEvent ige : events.values()) {
            if (ige.getEventType() == type) {
                return ige;
            }
        }
        return null; // shouldnt reach this point
    }

    public boolean charInEventMap(int charId) {
        InGameEvent e = getActiveEvent();

        if (e == null) {
            return false;
        }

        return e.charInEvent(charId);
    }
}
